﻿using System.Collections.Generic;

namespace Demo.Application.Shared.Estates
{
    public interface IEstateService
    {
        public void Create(CreateEstateInput input);

        public void Update(int id, UpdateEstateInput input);

        public IEnumerable<EstateDto> GetAll();

        public EstateDto GetById(int id);

        void Delete(int id);
    }
}