﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Application.Shared.Estates
{
    public class CreateEstateInput
    {
        [Required(ErrorMessage = "Please inter {0}")]
        [StringLength(512, ErrorMessage = "Address length should not be be less than {1} more than{0}", MinimumLength = 5)]
        public string Address { get; set; }

        public double Area { get; set; }

        public bool IsNorth { get; set; }

        [Required(ErrorMessage = "Please inter landlord")]
        public int LandLordId { get; set; }

        public int NO { get; set; }
    }
}