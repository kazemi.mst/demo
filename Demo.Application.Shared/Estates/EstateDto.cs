﻿namespace Demo.Application.Shared.Estates
{
    public class EstateDto
    {
        public int Id { get; set; }

        public long? CreatorUserId { get; set; }

        public string Address { get; set; }
    }
}