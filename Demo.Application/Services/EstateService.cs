﻿using Demo.Application.Shared.Estates;
using Demo.Core.Entities;
using Demo.Framework.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Demo.Application.Services
{
    public class EstateService : IEstateService
    {
        private readonly IRepository<Estate> _repository;

        public EstateService(IRepository<Estate> repository)
        {
            _repository = repository;
        }

        public void Create(CreateEstateInput input)
        {
            //Todo Use automapper
            _repository.Add(new Estate
            {
                Address = input.Address,
                Area = input.Area,
                IsNorth = input.IsNorth,
                LandLordId = input.LandLordId,
                NO = input.NO,
            });
        }

        public void Delete(int id)
        {
            var estate = _repository.GetSingle(a => a.Id == id);
            if (estate == null) throw new NullReferenceException($"estate with id : {id} not found");

            _repository.Delete(estate);
        }

        //ToDo use pagination
        public IEnumerable<EstateDto> GetAll()
        {
            //ToDo use select in join for better perfomance
            var estate = _repository.GetAll()
                .Include(a => a.LandLord);

            return estate.Select(a => new EstateDto
            {
                Id = a.Id,
                CreatorUserId = a.CreatorUserId,
                Address = a.Address,
            });
        }

        public EstateDto GetById(int id)
        {
            var estate = _repository.GetSingle(a => a.Id == id);
            return new EstateDto
            {
                Address = estate.Address,
                CreatorUserId = estate.CreatorUserId,
                Id = estate.Id
            };
        }

        public void Update([Required] int id, UpdateEstateInput input)
        {
            _repository.Update(new Estate
            {
                Id = id,
                Address = input.Address,
                Area = input.Area,
                IsNorth = input.IsNorth,
                LandLordId = input.LandLordId,
                NO = input.NO
            });
        }
    }
}