﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.EntityFrameworkCore.Migrations
{
    public partial class _2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Estate_LandLord_LandLordId",
                table: "Estate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LandLord",
                table: "LandLord");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Estate",
                table: "Estate");

            migrationBuilder.RenameTable(
                name: "LandLord",
                newName: "LandLords");

            migrationBuilder.RenameTable(
                name: "Estate",
                newName: "Estates");

            migrationBuilder.RenameIndex(
                name: "IX_Estate_LandLordId",
                table: "Estates",
                newName: "IX_Estates_LandLordId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LandLords",
                table: "LandLords",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Estates",
                table: "Estates",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Estates_LandLords_LandLordId",
                table: "Estates",
                column: "LandLordId",
                principalTable: "LandLords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Estates_LandLords_LandLordId",
                table: "Estates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LandLords",
                table: "LandLords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Estates",
                table: "Estates");

            migrationBuilder.RenameTable(
                name: "LandLords",
                newName: "LandLord");

            migrationBuilder.RenameTable(
                name: "Estates",
                newName: "Estate");

            migrationBuilder.RenameIndex(
                name: "IX_Estates_LandLordId",
                table: "Estate",
                newName: "IX_Estate_LandLordId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LandLord",
                table: "LandLord",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Estate",
                table: "Estate",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Estate_LandLord_LandLordId",
                table: "Estate",
                column: "LandLordId",
                principalTable: "LandLord",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
