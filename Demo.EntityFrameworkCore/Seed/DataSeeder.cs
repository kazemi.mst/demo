﻿using Demo.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demo.EntityFrameworkCore.Seed
{
    public class DataSeeder
    {
        public static void SeedLandLords(DemoDbContext context)
        {
            if (!context.LandLords.Any())
            {
                var countries = new List<LandLord>
                {
                    new LandLord {
                        FirstName = "میلاد",
                        SureName = "محمدی",
                        PhoneNumber = "+9821223223223",
                    },
                  new LandLord {
                        FirstName = "حسن",
                        SureName = "باقری",
                        PhoneNumber = "+9821555223223",
                    },
                  new LandLord {
                        FirstName = "میثم",
                        SureName = "ترابی",
                        PhoneNumber = "+9821666223223",
                    },
                };
                context.AddRange(countries);
                context.SaveChanges();
            }
        }
    }
}