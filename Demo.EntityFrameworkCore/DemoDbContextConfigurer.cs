﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using System.IO;

namespace Demo.EntityFrameworkCore
{
    public static class DemoDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DemoDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DemoDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection.ConnectionString);
        }
    }

    public class DemoDbContextFactory : IDesignTimeDbContextFactory<DemoDbContext>
    {
        public DemoDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DemoDbContext>();
            IConfiguration config = new ConfigurationBuilder()
              .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../Demo.Web"))
              .AddJsonFile("appsettings.json")
              .Build();

            DemoDbContextConfigurer.Configure(builder, config.GetConnectionString("Default"));

            return new DemoDbContext(builder.Options);
        }
    }
}