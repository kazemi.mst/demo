﻿using Demo.Core.Entities;
using Demo.EntityFrameworkCore.Configurations;
using Demo.Framework.EF.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Demo.EntityFrameworkCore
{
    public class DemoDbContext : DbContext
    {
        public DbSet<LandLord> LandLords { get; set; }

        public DbSet<Estate> Estates { get; set; }

        public DemoDbContext(DbContextOptions<DemoDbContext> options)
         : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EstateModelMapping).Assembly);
        }

        public override int SaveChanges()
        {
            // get entries that are being Added or Updated
            var modifiedEntries = ChangeTracker.Entries()
                    .Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));

            long userId = 0  /*Get from claims*/;
            var now = DateTime.Now;

            foreach (var entry in modifiedEntries)
            {
                var entity = entry.Entity as AuditedEntity;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatorUserId = userId;
                    entity.CreationTime = now;
                }

                entity.LastModificationTime = now;
                entity.LastModifierUserId = userId;
            }

            return base.SaveChanges();
        }
    }
}