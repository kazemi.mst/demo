﻿using Demo.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Demo.EntityFrameworkCore.Configurations
{
    public class LandLordModelMapping : IEntityTypeConfiguration<LandLord>
    {
        public void Configure(EntityTypeBuilder<LandLord> builder)
        {
            builder.Property(c => c.FirstName).HasMaxLength(255).IsRequired();
            builder.Property(c => c.SureName).HasMaxLength(255).IsRequired();
            builder.Property(c => c.PhoneNumber).HasMaxLength(20);

            builder.HasQueryFilter(a => !a.IsDeleted);
        }
    }
}