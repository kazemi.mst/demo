﻿using Demo.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Demo.EntityFrameworkCore.Configurations
{
    public class EstateModelMapping : IEntityTypeConfiguration<Estate>
    {
        public void Configure(EntityTypeBuilder<Estate> builder)
        {
            builder.Property(c => c.NO).HasMaxLength(255).IsRequired();
            builder.Property(c => c.Address).HasMaxLength(512);

            builder.HasOne(a => a.LandLord).WithMany(a => a.Estates)
                .HasForeignKey(a => a.LandLordId).IsRequired();

            builder.HasQueryFilter(a => !a.IsDeleted);
        }
    }
}