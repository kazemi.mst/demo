﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Application.Shared.Estates;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstateController : ControllerBase
    {
        private readonly IEstateService _estateService;

        public EstateController(IEstateService estateService)
        {
            _estateService = estateService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EstateDto>> GetAll()
        {
            var model = _estateService.GetAll();
            return Ok(model);
        }

        [HttpPost]
        public ActionResult Create([FromBody] CreateEstateInput input)
        {
            _estateService.Create(input);
            return Ok();
        }

        [HttpPut("{id}")]
        public ActionResult Update([FromRoute] int id, [FromBody] UpdateEstateInput input)
        {
            _estateService.Update(id, input);
            return Ok();
        }

        [HttpGet("{id}")]
        public ActionResult GetById([FromRoute] int id)
        {
            var model = _estateService.GetById(id);
            return Ok(model);
        }

        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] int id)
        {
            _estateService.Delete(id);
            return Ok();
        }
    }
}