﻿using Demo.EntityFrameworkCore;
using Demo.EntityFrameworkCore.Seed;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Demo.Web
{
    public static class Extentions
    {
        public static IHost SeedData(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<DemoDbContext>();

                DataSeeder.SeedLandLords(context);
            }
            return host;
        }
    }
}