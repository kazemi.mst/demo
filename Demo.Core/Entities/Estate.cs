﻿using Demo.Framework.EF.Entity;

namespace Demo.Core.Entities
{
    public class Estate : AuditedEntity, IEntity<int>, ISoftDelete
    {
        #region + Constroctor

        public Estate()
        {
        }

        #endregion + Constroctor

        #region + Property

        public int Id { get; set; }

        public int NO { get; set; }

        public double Area { get; set; }

        public string Address { get; set; }

        public bool IsNorth { get; set; }

        public int LandLordId { get; set; }

        public bool IsDeleted { get; set; }

        #endregion + Property

        #region + NavigationProperty

        public LandLord LandLord { get; set; }

        #endregion + NavigationProperty
    }
}