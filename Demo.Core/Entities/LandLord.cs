﻿using Demo.Framework.EF.Entity;
using System.Collections.Generic;

namespace Demo.Core.Entities
{
    public class LandLord : AuditedEntity, IEntity<int>, ISoftDelete
    {
        #region + Constroctor

        public LandLord()
        {
            Estates = new HashSet<Estate>();
        }

        #endregion + Constroctor

        #region + Property

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string SureName { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsDeleted { get; set; }

        #endregion + Property

        #region + NavigationProperty

        public ICollection<Estate> Estates { get; set; }

        #endregion + NavigationProperty
    }
}