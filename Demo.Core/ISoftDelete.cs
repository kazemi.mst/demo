﻿namespace Demo.Core
{
    public interface ISoftDelete
    {
        bool IsDeleted { get; set; }
    }
}