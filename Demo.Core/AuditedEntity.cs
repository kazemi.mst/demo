﻿using System;

namespace Demo.Framework.EF.Entity
{
    public abstract class AuditedEntity
    {
        protected AuditedEntity()
        {
        }

        public virtual DateTime? LastModificationTime { get; set; }

        public virtual long? LastModifierUserId { get; set; }

        public virtual DateTime? CreationTime { get; set; }

        public virtual long? CreatorUserId { get; set; }
    }
}