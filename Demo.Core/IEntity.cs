﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Core
{
    public interface IEntity<TKey>
    {
        public TKey Id { get; set; }
    }
}